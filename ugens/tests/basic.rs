#[cfg(test)]
mod ugens_struct {
    use ugens::Sin;

    #[test]
    fn sin() {
        let _sin = Sin::new(0.0, 0.0);
    }
    fn line() {
        let _sin = Sin::new(0.0, 0.0);
    }
}

#[cfg(test)]
mod test {
    use ugens::{Gettable, Line, Scalar, Ugen};

    #[test]
    fn basic() {
        let bus = Scalar::new(1.0);
        let mut a = Line::new(Line::new(bus, 0f32), 0f32);
        let b = a.busses("line");
        assert_eq!(1, b.keys().len());
        assert_eq!(b.keys().next().unwrap(), "line speed speed");
    }

    #[test]
    fn ugens() {
        let bus = Ugen(Scalar::new(1.0)) + Ugen(Scalar::new(2.0));
        let mut a = Line::new(Line::new(bus, 0f32), 0.0);
        let b = a.busses("line");
        let mut keys = b.keys();
        assert_eq!(2, keys.len());
        println!("{:?}", b.keys());
        assert_eq!(keys.next().unwrap(), "line speed speed add l ");
        assert_eq!(keys.next().unwrap(), "line speed speed add r ");
    }
}
