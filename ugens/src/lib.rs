#![feature(associated_type_defaults)]
#![feature(specialization)]
#![feature(trait_alias)]
#![feature(associated_type_bounds)]
#![feature(tau_constant)]
#![allow(warnings)]

//TODO
// implement hard limit for scalar controls
// [] notation for multiple output ? Channels output...
// control - audio rate => maybe with tokio later ?
// benchmarks
// TODO: interruptables

extern crate ugen_factory;

mod input;
mod traits;
mod ugens;

pub use input::*;
pub use serde::{Deserialize, Serialize};
pub use std::{
    f32::consts::PI,
    f32::consts::TAU,
    iter::{repeat, Repeat},
    ops::{Add, Div, Mul, Rem, Sub},
};
pub use traits::*;
use ugen_factory::new_ugen;
pub use ugens::*;
