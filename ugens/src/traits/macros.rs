#[macro_export]
macro_rules! op {
    ($type:ident<$($gen:ident:$trait:ident$(<$item:ident=$prim:ty>)?),*>,) => {
        op!{*, Mul, mul, MulUgen, MulUgenRef, $type<$($gen:$trait$(<$item=$prim>)?),*>,}
        op!{/, Div, div, DivUgen, DivUgenRef, $type<$($gen:$trait$(<$item=$prim>)?),*>,}
        op!{-, Sub, sub, SubUgen, SubUgenRef, $type<$($gen:$trait$(<$item=$prim>)?),*>,}
        op!{+, Add, add, AddUgen, AddUgenRef, $type<$($gen:$trait$(<$item=$prim>)?),*>,}
        op!{%, Rem, rem, RemUgen, RemUgenRef, $type<$($gen:$trait$(<$item=$prim>)?),*>,}
        impl<$($gen:$trait$(<$item=$prim>)?),*> UgenTrait for $type<$($gen),*> {}
    };
    ($op:tt, $trait_op:ident, $trait_fn:ident, $output:ident, $outputRef:ident,
    $type:ident<$($gen:ident:$trait:ident$(<$item:ident=$prim:ty>)?),*>,
     ) => {
        impl<U: UgenTrait, $($gen:$trait$(<$item=$prim>)?),*> $trait_op<U> for $type<$($gen),*>{
             type Output = $output<$type<$($gen),*>, U>;
             fn $trait_fn(self, rhs: U) -> Self::Output {
                $output(self, rhs)
             }
        }
        impl<'a, U: Iterator<Item = f32> + Debug, $($gen:$trait$(<$item=$prim>)?),*> $trait_op<U> for &'a mut $type<$($gen),*>{
            type Output = $outputRef<'a, $type<$($gen),*>, U>;
            fn $trait_fn(self, rhs: U) -> Self::Output {
                $outputRef(self, rhs)
            }
        }
        impl<$($gen:$trait$(<$item=$prim>)?),*> $trait_op<f32> for $type<$($gen),*>{
             type Output = $output<$type<$($gen),*>, Repeat<f32>>;
             fn $trait_fn(self, rhs: f32) -> Self::Output {
                $output(self, repeat(rhs))
             }
        }
        impl<$($gen:$trait$(<$item=$prim>)?),*> $trait_op<$type<$($gen),*>> for f32{
             type Output = $output<Repeat<f32>, $type<$($gen),*>>;
             fn $trait_fn(self, rhs: $type<$($gen),*>) -> Self::Output {
                $output(repeat(self), rhs)
             }
        }
    };
}

#[macro_export]
macro_rules! impl_op {
    ($trait:ident, $trait_fn:ident, $name:ident,
     $name_ref:ident, $name_ref_ref:ident,
     $one:ident, $two:ident, $op:expr) => {
        #[derive(Debug, Copy, Clone)]
        pub struct $name<I: Iterator, J: Iterator>(pub I, pub J);
        #[derive(Debug)]
        pub struct $name_ref<'a, I: Iterator, J: Iterator>(pub &'a mut I,pub J);
        #[derive(Debug)]
        pub struct $name_ref_ref<'a, 'b, I: Iterator, J: Iterator>(&'a mut I, &'b mut J);

        impl<U: $trait, I: Iterator<Item = U>, J: Iterator<Item = U>> Gettable for $name<I, J> {
            fn params(&mut self) -> Vec<(&str, &mut dyn Gettable)> {
                vec![(stringify!($trait_fn l), &mut self.0),
                (stringify!($trait_fn r), &mut self.1)]
            }
        }
        impl<U: $trait, I: Iterator<Item = U>, J: Iterator<Item = U>> Iterator for $name<I, J> {
            type Item = U::Output;
            fn next(&mut self) -> Option<Self::Item> {
                match (self.0.next(), self.1.next()) {
                    (Some($one), Some($two)) => $op,
                    _ => None,
                }
            }
        }
        impl<'a, U: $trait, I: Iterator<Item = U>, J: Iterator<Item = U>> Iterator for $name_ref<'a, I, J> {
            type Item = U::Output;
            fn next(&mut self) -> Option<Self::Item> {
                match (self.0.next(), self.1.next()) {
                    (Some($one), Some($two)) => $op,
                    _ => None,
                }
            }
        }
    };
}
