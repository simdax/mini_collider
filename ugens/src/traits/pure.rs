use crate::*;
use std::iter::repeat;

#[derive(Debug, Copy, Clone)]
pub struct DistortUgen<I: Iterator + Debug>(pub I);
impl<I: Iterator<Item = f32> + Debug> Iterator for DistortUgen<I> {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        match self.0.next() {
            Some(val) => {
                if val < 0.0 {
                    Some(val / (1.0 - val))
                } else {
                    Some(val / 1.0 + val)
                }
            }
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct RangeUgen<I: Iterator + Debug, J: Iterator, K: Iterator>(pub I, pub J, pub K);

pub fn range_f(sig: f32, max: f32, min: f32) -> f32 {
    min + (sig + 1.0) * ((max - min) / 2.0)
}

impl<I: Iterator<Item = f32> + Debug, J: Iterator<Item = f32>, K: Iterator<Item = f32>> Iterator
    for RangeUgen<I, J, K>
{
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        match (self.0.next(), self.1.next(), self.2.next()) {
            (Some(sig), Some(min), Some(max)) => Some(range_f(sig, min, max)),
            _ => None,
        }
    }
}

anonymous_ugen!(RangeUgen<I: f_input, J: f_input, K: f_input>);
anonymous_ugen!(DistortUgen<I: f_input>);

#[test]
fn range() {
    let mut a = Ugen(repeat(1.0)).range(0.3, 0.5);
    assert_eq!(a.next().unwrap(), 0.5);
    let mut a = Ugen(repeat(-1.0)).range(0.3, 0.5);
    assert_eq!(a.next().unwrap(), 0.3);
    let mut a = Ugen(repeat(0.0)).range(0.3, 0.5);
    assert_eq!(a.next().unwrap(), 0.4);
}
