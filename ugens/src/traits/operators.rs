use crate::*;

impl_op! { Mul, mul, MulUgen, MulUgenRef, MulUgenRefRef, x, y, Some(x*y) }
impl_op! { Sub, sub, SubUgen, SubUgenRef, SubUgenRefRef, x, y, Some(x-y) }
impl_op! { Add, add, AddUgen, AddUgenRef, AddUgenRefRef, x, y, Some(x+y) }
impl_op! { Div, div, DivUgen, DivUgenRef, DivUgenRefRef, x, y, Some(x/y) }
impl_op! { Rem, rem, RemUgen, RemUgenRef, RemUgenRefRef, x, y, Some(x%y) }

op! {AddUgen<T: f_input, V: f_input >,}
op! {SubUgen<T: f_input, V: f_input >,}
op! {MulUgen<T: f_input, V: f_input >,}
op! {DivUgen<T: f_input, V: f_input >,}
op! {RemUgen<T: f_input, V: f_input >,}

#[cfg(test)]
mod test {
    use super::*;
    use std::iter::repeat;

    #[test]
    fn operators() {
        // MulUgen(repeat(1), repeat(1));
        // Sin {
        //     phasor: repeat(440.0),
        //     amp: repeat(1.0),
        // } * 4.0;
    }

    #[test]
    fn ugen_base() {
        // let mut m = Ugen(0..4) * (1..4);
        // assert_eq!(m.next().unwrap(), 0);
        // assert_eq!(m.next().unwrap(), 2);
        // assert_eq!(m.next().unwrap(), 6);
        // assert_eq!(m.next(), None);
    }

    #[test]
    fn ugen_ref() {
        // let mut ugen = Ugen(0..10);
        // let mut m = &mut ugen * (1..4);
        // assert_eq!(m.next().unwrap(), 0);
        // assert_eq!(m.next().unwrap(), 2);
        // assert_eq!(m.next().unwrap(), 6);
        // assert_eq!(m.next(), None);
        // // here the ref was consumed so we have one offset
        // let mut m = &mut ugen + (1.0..4.0);
        // assert_eq!(m.next().unwrap(), 5);
        // assert_eq!(m.next().unwrap(), 7);
        // assert_eq!(m.next().unwrap(), 9);
        // assert_eq!(m.next(), None);
    }

    #[test]
    fn ugen_iterator() {
        // let mut m = Ugen(0f32..4.0) * Ugen(1f32..4.0);
        // assert_eq!(m.next().unwrap(), 0);
        // assert_eq!(m.next().unwrap(), 2);
        // assert_eq!(m.next().unwrap(), 6);
        // assert_eq!(m.next(), None);
    }

    #[test]
    fn ugen_ugen() {
        // let mut m = Ugen(Line::scalar(2.0)) * Ugen(Line::scalar(3.0));
        // assert_eq!(0.0, m.next().unwrap());
        // assert_eq!(6.0, m.next().unwrap());
    }
}
