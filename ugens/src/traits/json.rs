extern crate serde_json;
use serde::Serialize;

pub trait Json {
    fn json(&self) -> String;
}

impl<T: Serialize> Json for T {
    fn json(&self) -> String {
        serde_json::to_string(&self).expect("can't deserialize")
    }
}
