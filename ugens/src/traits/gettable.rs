use crate::input::Bus;
use crate::Value;
use std::collections::HashMap;

pub trait Gettable {
    fn params(&mut self) -> Vec<(&str, &mut dyn Gettable)>;
    fn get(&mut self, name: &str) -> Vec<Option<(String, Value)>>;
    fn busses(&mut self, name: &str) -> HashMap<String, Bus> {
        let mut busses: HashMap<String, Bus> = HashMap::new();
        self.get(name)
            .into_iter()
            .map(|x| x.unwrap())
            .for_each(|(a, b)| {
                match busses.get_mut(&a) {
                    Some(vec) => {
                        vec.0.push(b);
                    }
                    None => {
                        busses.insert(a, Bus(vec![b]));
                    }
                };
            });
        busses
    }
}

impl<T: Iterator> Gettable for T {
    default fn params(&mut self) -> Vec<(&str, &mut dyn Gettable)> {
        // println!("dummy param");
        vec![]
    }
    default fn get(&mut self, name: &str) -> Vec<Option<(String, Value)>> {
        self.params()
            .iter_mut()
            .flat_map(|(n, input)| {
                if name.is_empty() {
                    input.get(&format!("{}", n))
                } else {
                    input.get(&format!("{} {}", name, n))
                }
            })
            .collect()
    }
}
