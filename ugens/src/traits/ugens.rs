use crate::*;
use std::iter::{repeat, Repeat};

#[macro_export]
macro_rules! anonymous_ugen {
    ($type:ident<$($generic:ident:$trait:ident),*>) => {
        impl<$($generic:$trait),*> Gettable for $type<$($generic),*> {
            fn params(&mut self) -> Vec<(&str, &mut dyn Gettable)> {
                vec![("", &mut self.0)]
            }
        }
        op!($type<$($generic:$trait),*>,);
    }
}

pub trait UgenTrait:
    Iterator<Item = f32> + Clone + Debug + Mul + Div + Sub + Add + Rem + std::marker::Sized
{
    fn distort(self) -> DistortUgen<Self> {
        DistortUgen(self)
    }
    fn range<
        Jitem: Iterator,
        J: Input<Iterator = Jitem>,
        Kitem: Iterator,
        K: Input<Iterator = Kitem>,
    >(
        self,
        min: J,
        max: K,
    ) -> RangeUgen<Self, Jitem, Kitem> {
        RangeUgen(self, min.input(), max.input())
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Ugen<I>(pub I);

impl<I: Iterator> Iterator for Ugen<I> {
    type Item = I::Item;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

anonymous_ugen!(Ugen<I: f_input>);
