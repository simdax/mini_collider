use crate::*;

pub trait Interrupt: Iterator {
    fn toggle();
}
#[derive(Clone, Debug)]
pub struct Interruptable<I: f_input>(pub I, pub Receiver<()>);

impl<I: f_input> Iterator for Interruptable<I> {
    type Item = I::Item;
    fn next(&mut self) -> Option<Self::Item> {
        if self.1.try_recv().is_err() {
            self.0.next()
        } else {
            None
        }
    }
}
