use crate::traits::UgenTrait;
use crate::Scalar;
use std::iter::{repeat, Repeat};

pub trait Input {
    type Iterator;
    type Item;
    fn input(self) -> Self::Iterator;
}

impl<T: UgenTrait> Input for T {
    type Iterator = Self;
    type Item = T::Item;
    fn input(self) -> Self::Iterator {
        self
    }
}

macro_rules! input {
    () => {};
    ($type:ty, $($t:ty),*) => {
        input!($type);
        input!($($t),*);
    };
    ($type:ty) => {
        impl Input for $type {
            type Iterator = Repeat<$type>;
            type Item = $type;
            fn input(self) -> Self::Iterator {
                repeat(self)
            }
        }
    };
}

input!(f32, u32, bool);
