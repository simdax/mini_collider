use serde::{Serialize, Serializer};

#[derive(Debug, Copy, Clone)]
pub struct Value(pub *mut f32);
impl Serialize for Value {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let value = unsafe { *self.0 };
        serializer.serialize_f32(value)
    }
}
unsafe impl Send for Value {}

impl Value {
    pub fn send(&self, value: f32) {
        unsafe {
            *self.0 = value;
        }
    }
}

// pub type Bus = Vec<Value>;
#[derive(Clone, Debug)]
pub struct Bus(pub Vec<Value>);

impl Bus {
    pub fn send(&self, v: f32) {
        for val in &self.0 {
            val.send(v);
        }
    }
}

impl Serialize for Bus {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        self.0[0].serialize(serializer)
    }
}
