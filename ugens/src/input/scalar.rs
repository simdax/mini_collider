use crate::*;
use serde::Serializer;
use std::collections::HashMap;

#[derive(Debug, Copy, Clone)]
pub struct Scalar(f32, pub Value, &'static str);

impl Scalar {
    pub fn new(value: f32, name: &'static str) -> Self {
        let mut s = Self(value, Value(std::ptr::null_mut::<f32>()), name);
        s.refresh();
        s
    }
    fn refresh(&mut self) {
        self.1 = Value(&mut self.0 as *mut f32);
    }
}
impl Iterator for Scalar {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        Some(self.0)
    }
}
impl Serialize for Scalar {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_f32(self.0)
    }
}
impl PartialEq for Scalar {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl Gettable for Scalar {
    fn params(&mut self) -> Vec<(&str, &mut dyn Gettable)> {
        unimplemented!("a scalar cannot have parameters")
    }
    fn get(&mut self, name: &str) -> Vec<Option<(String, Value)>> {
        self.refresh();
        if self.2.is_empty() {
            vec![Some((name.to_string(), self.1)); 1]
        } else {
            vec![Some((self.2.to_string(), self.1)); 1]
        }
    }
}
op! {Scalar<>,}

#[cfg(test)]
mod test {
    use crate::{Phasor, Scalar};
    use std::time::Duration;

    #[test]
    fn serialize_scalar() {
        use crate::traits::json::Json;
        let a = Scalar::new(43.0, "");
        let json = a.json();
        assert_eq!(json, "43.0");
        let phasor = Phasor::new(Scalar::new(43.0, ""));
        assert_eq!(phasor.json(), "{\"freq\":43.0}");
        let phasor = Phasor::new(Scalar::new(43.0, "coucou"));
        assert_eq!(phasor.json(), "{\"coucou\":43.0}");
    }

    #[test]
    fn ugen_clone() {
        let mut line = Scalar::new(1.0, "");
        let mut line2 = line;
        line.refresh();
        line2.refresh();
        println!("{:?} {:?}", line, line2);
        let mut op = line + line2;
        assert_eq!(line.next().unwrap(), 1.0);
        assert_eq!(line2.next().unwrap(), 1.0);
        assert_eq!(op.next().unwrap(), 2.0);
        line.1.send(2.0);
        assert_eq!(line.next().unwrap(), 2.0);
        assert_eq!(line2.next().unwrap(), 2.0);
        assert_eq!(op.next().unwrap(), 4.0);
        line2.1.send(1.0);
        assert_eq!(line.next().unwrap(), 1.0);
        assert_eq!(line2.next().unwrap(), 1.0);
        assert_eq!(op.next().unwrap(), 2.0);
    }

    #[test]
    fn ugen_calculus() {
        let mut line = Scalar::new(0.1, "");
        let line2 = Scalar::new(0.2, "");
        let line3 = Scalar::new(0.3, "");
        let line4 = Scalar::new(0.4, "");
        let mut io = line * line2 - line3 / line4 + line;
        assert_eq!(io.next().unwrap(), 0.1 * 0.2 - 0.3 / 0.4 + 0.1);
        assert_eq!(io.next().unwrap(), 0.1 * 0.2 - 0.3 / 0.4 + 0.1);
        line.refresh();
        line.1.send(0.5);
        assert_eq!(line.next(), Some(0.5));
        // std::thread::sleep(Duration::from_millis(300));
        assert_eq!(io.next().unwrap(), 0.5 * 0.2 - 0.3 / 0.4 + 0.5);
        // let mut io = line * (line2 - line3) / line4 + line5;
    }
}
