#![allow(non_camel_case_types)]

pub use std::fmt::Debug;

pub trait iteraput = Iterator + Debug;
pub trait itf32 = Iterator<Item = f32>;
pub trait f_input = Iterator<Item = f32> + Debug + Clone;
pub trait i_input = Iterator<Item = i32> + Debug;

mod macros;
mod structs;

pub use structs::*;
