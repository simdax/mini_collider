#[macro_export]
macro_rules! ugen {
    (
        $self:ident,
        $type:ident (
            $($gen:ident),*
            $(public: $($gen2:ident:$ty2:ty),* | )?
            $(private: $($private_field:ident:$pr_type:ty=$value:expr),*)?
        ) =>
        $(Item = $item_type:ty,)?
        { $($inner:tt)* }
    ) => {
        ugen_struct!(
            $type<$($gen:f_input),*>,
            $(public: $($gen2:$ty2),* | )?,
            $($($private_field:$pr_type=$value),*)?
        );
        ugen_impl!(
            $self,
            $type<$($gen:f_input),*>,
            $(Item = $item_type,)?
            { $($inner)* }
        );
    }
}

#[macro_export]
macro_rules! ugen_struct {
    (
        $type:ident<$($gen:ident:$trait:ident$(<$item:ident=$prim:ty>)?),*>,
        $(public: $($gen2:ident:$ty2:ty),* | )?,
        $($private_field:ident:$pr_type:ty=$value:expr),*
    ) => {
        #[derive(Debug, Deserialize, Serialize, Clone)]
        pub struct $type<$($gen:$trait$(<$item=$prim>)?),*>{
            $(pub $gen: $gen,)*
            $($($gen2: $ty2,)*)?
            $(
                #[serde(skip)]
                $private_field:$pr_type
            ),*
        }
        impl<$($gen:$trait$(<$item=$prim>)?),*> $type<$($gen),*> {
            new_ugen![ $type ($($gen)*) ($($($gen2:$ty2),*)?) {
                    $($gen:$gen.input(),)*
                    $($($gen2,)*)?
                    $(
                        $private_field:$value
                    ),*
            }];
        }
    }
}

#[macro_export]
macro_rules! ugen_impl {
    ($self:ident,
    $type:ident<$($gen:ident:$trait:ident$(<$item:ident=$prim:ty>)?),*>,
    $(Item = $item_type:ty,)?
    { $($inner:tt)* }
    ) => {
        macro_rules! default_item {
            () => {type Item = f32; };
            ($ty:ty) => {type Item = $ty;}
        }
        impl<$($gen:$trait$(<$item=$prim>)?),*> Gettable for $type<$($gen),*> {
            fn params(&mut $self) -> Vec<(&str, &mut dyn Gettable)> {
                vec![$((stringify!($gen), &mut $self.$gen)),*]
            }
        }
        impl<$($gen:$trait$(<$item=$prim>)?),*> Iterator for $type<$($gen),*> {
            default_item!($($item_type)?);
            fn next(&mut $self) -> Option<Self::Item> {
                $($inner)*
            }
        }
        op!{$type<$($gen:$trait$(<$item=$prim>)?),*>,}
    }
}
