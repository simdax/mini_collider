#[macro_export]
macro_rules! get_control {
    ($self:ident,) => {};
    ($self:ident, $control:ident, $($tail:ident),*) => {
         get_control!($self, $control);
         get_control!($self, $($tail),*);
    };
    ($self:ident, $control:ident) => {
        let $control = match $self.$control.next(){
            Some(value) => value,
            None => {
                return None;
            }
        };
    };
}

#[macro_export]
macro_rules! clamp {
    ($id:ident,$max:expr, $min:expr) => {
        if $id > $max {
            $max
        } else if $id < $min {
            $min
        } else {
            $id
        }
    };
}
