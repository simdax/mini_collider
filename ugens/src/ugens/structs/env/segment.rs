use crate::*;

ugen! {self, Segm(from, to, steps
// public: steps:u32 |
private: i:u32 = 0) => {
    get_control!(self, from, to, steps);
        let res = from + self.i as f32 * ((to - from) / steps);
        if (res - to).abs() > f32::EPSILON{
            self.i += 1;
            Some(res)
        } else {None}
}
}
