mod segment;

use crate::*;
pub use segment::*;
use std::iter::Chain;

pub struct Envelope<I: f_input>(pub Sender<()>, pub Receiver<()>, pub I);

impl<I: f_input> Envelope<Interruptable<I>> {
    pub fn new(iterator: I) -> Self {
        let (tx, rx) = channel();
        Envelope(tx, rx.clone(), Interruptable(iterator, rx))
    }
}
type Adsr<att, dec, sus, rel> = Ugen<
    Chain<
        Interruptable<
            Chain<Segm<Repeat<f32>, Repeat<f32>, att>, Chain<Segm<Repeat<f32>, sus, dec>, sus>>,
        >,
        Segm<sus, Repeat<f32>, rel>,
    >,
>;

pub fn adsr<
    att: f_input,
    dec: f_input,
    sus: f_input,
    rel: f_input,
    attI: Clone + Debug + Input<Iterator = att>,
    decI: Clone + Debug + Input<Iterator = dec>,
    susI: Clone + Debug + Input<Iterator = sus>,
    relI: Clone + Debug + Input<Iterator = rel>,
>(
    att: attI,
    dec: decI,
    sus: susI,
    rel: relI,
) -> Envelope<Adsr<att, dec, sus, rel>> {
    let (tx, rx) = channel();
    Envelope(
        tx,
        rx.clone(),
        Ugen(
            Interruptable(
                Segm::new(0.0, 1.0, att)
                    .chain(Segm::new(1.0, sus.clone(), dec).chain(sus.clone().input())),
                rx,
            )
            .chain(Segm::new(sus, 0.0, rel)),
        ),
    )
}

impl Default for Envelope<Adsr<Repeat<f32>, Repeat<f32>, Repeat<f32>, Repeat<f32>>> {
    fn default() -> Self {
        adsr(1000.0, 1000.0, 0.8, 1000.0)
    }
}
