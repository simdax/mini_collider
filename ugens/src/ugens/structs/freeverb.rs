use crate::*;

#[derive(Debug, Default, Clone)]
struct DelayLine {
    array: Vec<f32>,
    index: usize,
    mem: (f32, f32),
}

impl DelayLine {
    pub fn new(size: usize) -> Self {
        Self {
            array: vec![0.0; size],
            index: 0,
            mem: (0.0, 0.0),
        }
    }
    pub fn swap_one(&mut self, input: f32, room: f32, damp: f32) {
        self.inc();
        let t = self.array[self.index];
        self.mem.0 = (1.0 - damp) * self.mem.1 + damp * self.mem.0;
        self.array[self.index] = input + room * self.mem.0;
        self.mem.1 = t;
    }
    pub fn swap_two(&mut self, expr: f32) {
        self.inc();
        let t = self.array[self.index];
        self.array[self.index] = 0.5 * self.mem.0 + expr;
        self.mem.0 = t;
        self.mem.1 = self.mem.0 - expr;
    }
    fn inc(&mut self) {
        self.index += 1;
        if self.index == self.array.len() {
            self.index = 0
        }
    }
}

fn dlines(sizes: &[usize]) -> Vec<DelayLine> {
    sizes.iter().map(|size| DelayLine::new(*size)).collect()
}

ugen!(self, FreeVerb(
    input, mix, room, damp
    private:
        lines: (Vec<DelayLine>, Vec<DelayLine>) = (
            dlines(&[225, 341, 441, 556]),
            dlines(&[1617, 1557, 1491, 1422, 1277, 1116, 1188, 1356])
    )
) => {
        get_control!(self, mix, room, input, damp);
        let mix = clamp!(mix, 0.0, 1.0);
        let room = clamp!(room, 0.0, 1.0);
        let damp = clamp!(damp, 0.0, 1.0);

        let sample = self.lines.0.iter_mut().rev().fold(
            self.lines.1.iter_mut().map(|line| {
                line.swap_one(input * 1.5e-2, room * 0.28 + 0.7, damp * 0.4);
                line.mem.1
            }).sum(),
            |expr, line| { line.swap_two(expr); line.mem.1 },
        );
        Some((1.0 - mix) * input + mix * sample)
    }
);
