mod conversions;
use crate::*;
use conversions::{Sample, SampleRateConverter};
use cpal::SampleRate;
use rodio::{source::SamplesConverter, Decoder, Source};
use serde::export::Formatter;
use std::fmt::Debug;
use std::fs::File;
use std::io::BufReader;

trait sndFileIt = Iterator<Item: Sample + Clone>;
type reader = SamplesConverter<Decoder<BufReader<File>>, f32>;

pub struct SndFile {
    iter: SampleRateConverter<reader>,
    path: String,
    sr: SampleRate,
}

op!(SndFile<>,);

impl Clone for SndFile {
    fn clone(&self) -> Self {
        Self::new(self.path.as_ref(), self.sr)
    }
}
impl Debug for SndFile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("")
    }
}
impl Iterator for SndFile {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl SndFile {
    pub fn new(path: &str, sr: SampleRate) -> Self {
        let file = std::fs::File::open(path).unwrap();
        let decoder = rodio::Decoder::new(BufReader::new(file)).unwrap();
        let decoder_sr = SampleRate(decoder.sample_rate());
        let decoder_channels = decoder.channels();

        println!("new sound file");
        println!("sample rate : {:?}", decoder_sr);
        println!("channels: {}", decoder_channels);
        let decoder = decoder.convert_samples::<f32>();
        let converter = SampleRateConverter::new(decoder, decoder_sr, sr, decoder_channels);
        Self {
            iter: converter,
            path: path.to_owned(),
            sr,
        }
    }
}
