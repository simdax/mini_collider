use crate::*;
use std::collections::VecDeque;

type delay_queue = VecDeque<f32>;

ugen! { self, Delay(
    input
    public: delay:usize |
    private:
        mem: delay_queue = VecDeque::with_capacity(delay)
    ) =>
    {
        match self.input.next() {
            Some(input) => {
                self.mem.push_back(input);
                if self.delay > 0 {
                    self.delay -= 1;
                    Some(input)
                } else {
                    let delay = self.mem.pop_front().unwrap_or_default();
                    Some(input + (delay * 0.8))
                }
            }
            None => None,
        }
    }
}
