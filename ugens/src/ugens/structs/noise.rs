extern crate rand;
use crate::*;

ugen! { self, Noise() => {
    Some((rand::random::<f32>() - 0.5) * 2.0)
}
}

#[cfg(test)]
mod test {
    use crate::{Noise, UgenTrait};

    #[test]
    fn noise() {
        let ugen = Noise {};
        let mut sig = ugen.take(1000);
        assert!(sig.any(|x| x < 0.0));
        assert!(sig.any(|x| x > 0.0));
        assert!(sig.all(|x| x < 1.0 && x > -1.0))
    }

    #[test]
    fn noise_range() {
        let ugen = Noise {}.range(0.1, 0.5);
        let mut sig = ugen.take(1000);
        assert!(sig.any(|x| x > 0.45));
        assert!(sig.any(|x| x < 0.15));
        assert!(
            sig.all(|x| x > 0.1 && x < 0.5),
            format!("par ex: {:?}", sig.find(|x| *x < 0.1 || *x > 0.5))
        );
    }
}
