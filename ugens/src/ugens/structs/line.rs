use crate::*;

ugen! { self, Line(
    speed, offset
    private: i:u32 = 0) =>
    {
        let res = self.offset.next().unwrap() + self.i as f32 * self.speed.next().unwrap();
        self.i += 1;
        Some(res)
    }
}

#[cfg(test)]
mod test {
    use crate::Line;

    #[test]
    fn line() {
        let _ = Line::new(0.0, 0.0);
    }
}
