use crate::*;

ugen! { self, Phasor(
    freq
    private:
        sr:f32=48000.0, i:f32=0.0
    ) =>
    {
        self.i += self.freq.next().unwrap() * (TAU / self.sr);
        if self.i > PI { self.i -= TAU}
        Some(self.i)
    }
}
