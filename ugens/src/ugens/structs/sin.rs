use crate::*;

ugen! { self,
    Sin(phasor, amp) =>
    {
        Some(self.phasor.next().unwrap().sin() * self.amp.next().unwrap())
    }
}

extern crate fastapprox;
use fastapprox::faster::sin;

ugen! {self, FastSin(phasor, amp) => {
        let freq = self.phasor.next().unwrap();
        Some(sin(freq % TAU - PI) * self.amp.next().unwrap())
    }
}
