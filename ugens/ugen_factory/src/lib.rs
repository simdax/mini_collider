extern crate proc_macro;
use proc_macro::TokenTree::{Group, Ident};
use proc_macro::{TokenStream, TokenTree};

fn token_group<F: FnMut(TokenStream, usize)>(token: TokenTree, mut lambda: F) {
    if let Group(tts) = token {
        for (i, token) in tts.stream().into_iter().enumerate() {
            if let Group(t) = token {
                lambda(t.stream(), i);
            }
        }
    }
}

#[proc_macro]
pub fn new_ugen(s: TokenStream) -> TokenStream {
    let mut s = s.into_iter();
    let _title = s.next().unwrap().to_string();
    let mut stream = String::from("pub fn new<");
    let mut types = vec![];
    for token in s.next() {
        if let Group(tts) = token {
            for token in tts.stream() {
                if let Group(t) = token {
                    for token in t.stream() {
                        if let Ident(t) = token {
                            stream += &(t.to_string() + "_input");
                            stream += &(String::from(":Input<Iterator=") + &t.to_string() + ">,");
                            types.push(t);
                        }
                    }
                }
            }
        }
    }
    stream += ">(";
    for t in &types {
        stream += &(t.to_string() + ":" + &t.to_string() + "_input,");
    }
    token_group(s.next().unwrap(), |t, index| {
        if index % 4 == 0 {
            stream += &(t.to_string() + ":");
        } else {
            stream += &(t.to_string() + ",");
        }
    });
    stream += ") -> Self { Self";
    stream += &s.next().unwrap().to_string();
    stream += "}";
    // println!("creating ugen: {} {}", title, stream);
    stream.parse().unwrap()
}
