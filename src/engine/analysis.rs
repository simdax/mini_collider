// Perform a forward FFT of size 1234
use rustfft::num_complex::Complex;
use rustfft::num_traits::{FromPrimitive, Pow, Zero};
use rustfft::FFTplanner;
use std::sync::Arc;

pub fn fft(data: Vec<f32>) -> Vec<f32> {
    let len = data.len();
    let mut input: Vec<Complex<f32>> = data
        .iter()
        .map(|x| Complex::from_f32(*x).unwrap())
        .collect();
    let mut output: Vec<Complex<f32>> = vec![Complex::zero(); len];

    let mut planner = FFTplanner::new(false);
    let fft = planner.plan_fft(len);
    fft.process(&mut input, &mut output);
    output
        .iter()
        .map(|x| f32::sqrt(x.re.pow(2) * x.im.pow(2)))
        // .map(|x| x / f32::sqrt(len as f32))
        .collect()
}
