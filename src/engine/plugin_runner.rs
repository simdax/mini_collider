use crate::HashMap;
use plugins::PluginTrait;
use std::fmt::Formatter;
use ugens::{Bus, Debug};

pub struct PluginRunner(pub Box<dyn PluginTrait>);

impl PluginRunner {
    pub fn parameters(&self) -> HashMap<String, Bus> {
        self.0.parameters()
    }
}
impl Iterator for PluginRunner {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        Some(self.0.next())
    }
}
impl Clone for PluginRunner {
    fn clone(&self) -> Self {
        unimplemented!()
    }
}
impl Debug for PluginRunner {
    fn fmt(&self, _f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        unimplemented!()
    }
}
unsafe impl Send for PluginRunner {}
