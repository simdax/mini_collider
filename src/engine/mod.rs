mod analysis;
pub use analysis::fft;
pub(crate) mod plugin_runner;
mod utils;
pub use utils::copy_dll;
pub mod watch;

use crate::back::{sender_trait::SenderEnum, spawn::spawn3};
use crate::engine::plugin_runner::PluginRunner;
use crate::engine::utils::{get_most_recent, get_plugins};
use crate::master::Master;
use crate::synth::SynthApi;
use crate::{LIB_PATH, TMP_FOLDER};
use libloading::Library;
use plugins::Plugin;
use std::fs;
use std::path::PathBuf;
use ugens::Envelope;

pub struct MiniCollider {
    pub outs: Vec<SenderEnum>,
    pub plugins: Vec<String>,
    pub actual_ugen: Option<String>,
    pub actual_dll: PathBuf,
    pub lib: Library,
    pub api: Option<SynthApi>,
}

impl MiniCollider {
    pub fn new() -> Result<Self, String> {
        let audio = Master::default();
        let plugins = get_plugins();
        let actual_ugen = None;
        let actual_dll = {
            fs::create_dir(TMP_FOLDER).unwrap_or_else(|_e| println!());
            get_most_recent(TMP_FOLDER.parse().unwrap()).unwrap_or_else(|_| copy_dll(LIB_PATH))
        };
        let lib = Library::new(&actual_dll).map_err(|e| e.to_string())?;
        println!("//////////");
        println!("\t\tWelcome to Mini Collider !");
        println!("\t\tPlugins available: {:?}", plugins);
        println!("//////////");
        println!("\nHappy coding\n");
        Ok(Self {
            plugins,
            actual_ugen,
            actual_dll,
            lib,
            outs: audio.outs(),
            api: None,
        })
    }

    pub fn start_plugin(&mut self) -> Result<(), String> {
        let name = match &self.actual_ugen {
            Some(n) => n,
            None => {
                return Err(String::from("no actual ugen"));
            }
        };
        let ugen = unsafe {
            let ugen_builder = self
                .lib
                .get::<extern "C" fn() -> Plugin>(name.as_bytes())
                .map_err(|e| e.to_string())?;
            let ugen = Box::from_raw(ugen_builder());
            PluginRunner(*ugen)
        };
        let params = ugen.parameters();
        let env = Envelope::default();
        let mut synth = spawn3(self.outs.clone(), ugen, env);
        synth.api.params = params;
        self.api = Some(synth.api);
        println!("{:?}", self.api);
        Ok(())
    }

    pub fn stop_plugin(&self) {
        self.api.as_ref().and_then(|api| api.stop().ok());
    }

    pub fn refresh(&mut self, dll: PathBuf) {
        self.lib = Library::new(&dll)
            .map_err(|e| e.to_string())
            .expect("can't create lib");
        self.actual_dll = dll;
        self.plugins = get_plugins();
        println!(
            "dll and plugins refreshed: {:?} {:?}",
            self.actual_dll, self.plugins
        );
    }
}
