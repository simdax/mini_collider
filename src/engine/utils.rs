use crate::{PLUGINS_LIST_PATH, TMP_FOLDER};
use std::fs;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::time::SystemTime;

pub fn get_most_recent(path: PathBuf) -> Result<PathBuf, String> {
    let folder = format!("{}/*", path.to_str().unwrap());
    let shared_libs = glob::glob(folder.as_str()).unwrap();
    let mut times = shared_libs
        .map(|path| {
            let path = &path.unwrap();
            let time = Path::metadata(&path.clone()).unwrap().created().unwrap();
            (path.clone(), time)
        })
        .collect::<Vec<(PathBuf, SystemTime)>>();
    times.sort_by(|a, b| a.1.cmp(&b.1));
    let times = times.last().unwrap();
    Ok(PathBuf::from(&times.0))
}

pub fn get_plugins() -> Vec<String> {
    File::open(PLUGINS_LIST_PATH)
        .map(|file| BufReader::new(file).lines())
        .expect("")
        .map(|x| x.unwrap())
        .collect::<Vec<String>>()
}

pub fn copy_dll(path: &str) -> PathBuf {
    let mut dll_path = PathBuf::from(TMP_FOLDER);
    dll_path.push(format!(
        "{}",
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs()
    ));
    dll_path.set_extension("dll");
    fs::copy(path, &dll_path).unwrap_or_else(|_| panic!("can't copy to {:?}", dll_path));
    dll_path
}
