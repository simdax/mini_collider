#![feature(specialization)]
#![feature(stmt_expr_attributes)]
#![allow(irrefutable_let_patterns)]
#![allow(warnings)]

pub const PLUGINS_LIST_PATH: &str = "plugins/.plugins";
pub const TMP_FOLDER: &str = ".tmp";
pub const LIB_PATH: &str = "plugins/target/debug/plugins.dll";
pub const SERVER_URL: &str = "localhost:8000";

mod back;
mod engine;
mod front;

pub use crate::back::*;
pub use crate::engine::*;
pub use crate::front::*;
pub use plugins::*;
pub use ugens::{json::Json, Bus, Scalar, Value};

pub use crossbeam::Sender;
pub use std::collections::HashMap;

pub mod prelude;
