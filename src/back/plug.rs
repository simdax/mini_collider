use crate::back::synth::Synth;
use crate::master::Master;
use std::thread::JoinHandle;

pub trait Plug {
    fn plug(self, rx: &Synth) -> JoinHandle<()>;
}

impl Plug for Master {
    fn plug(self, _synth: &Synth) -> JoinHandle<()> {
        // let rx = synth.channel.clone();
        std::thread::spawn(move || loop {
            // for ch in &[self.out, self.out] {
            //     let v = rx.try_recv().unwrap_or_default();
            //     ch.send(v).expect("rx is dead");
            // }
        })
    }
}
