use crate::back::sender_trait::SenderEnum;
use cpal::{
    traits::*,
    SampleRate,
    // Stream
};
use std::iter::repeat;
use std::sync::mpsc::{sync_channel, SyncSender};
use std::time::Duration;

#[derive(Clone)]
pub struct Master {
    pub out: SyncSender<f32>,
    pub sr: SampleRate,
    pub chans: usize,
    // pub stream: Stream,
}

impl Master {
    pub fn outs(&self) -> Vec<SenderEnum> {
        let out = SenderEnum::Master(self.out.clone());
        repeat(out).take(self.chans).collect()
    }
}

impl Default for Master {
    fn default() -> Self {
        let host = cpal::default_host();
        let device = host.default_output_device().expect("no device");
        let conf = device.default_output_config().expect("no conf");

        let (tx, rx) = sync_channel(4000);
        let nb_chan = conf.channels() as usize;
        let sr = conf.sample_rate();
        println!("device sample rate: {:?}", sr);
        println!("device channels: {}", nb_chan);
        std::thread::spawn(move || {
            let stream = device
                .build_output_stream(
                    &conf.into(),
                    move |data: &mut [f32], _| {
                        for frame in data.chunks_mut(nb_chan) {
                            for sample in frame.iter_mut() {
                                *sample = rx.try_recv().unwrap_or_default();
                            }
                        }
                    },
                    |_| panic!(),
                )
                .expect("no stream");
            stream.play().expect("can't play");
            loop {
                std::thread::sleep(Duration::from_secs(1000));
            }
        });
        Self {
            sr,
            chans: nb_chan,
            // stream,
            out: tx,
        }
    }
}
