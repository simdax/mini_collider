use crate::back::sender_trait::{SenderEnum, SenderTrait};
use crate::back::synth::{Synth, SynthApi};
use crate::plugin_runner::PluginRunner;
use crossbeam::{bounded as sync_channel, unbounded as channel};
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use ugens::*;

pub trait Spawner<U, T: Iterator<Item = U>, Env: Iterator<Item = U>> {
    fn spawn(self, size: usize, env: Env, pause: Receiver<()>, toggle_pause: Sender<()>) -> Synth;
}

fn pr_spawn<T: f_input + Clone + Send + 'static, Env: f_input + Clone + Send + 'static>(
    sig: T,
    stop: Receiver<()>,
    pause: Receiver<()>,
    tx_audio: Sender<f32>,
    bus_sender: Sender<HashMap<String, Bus>>,
    env: Env,
) {
    std::thread::spawn(move || {
        let mut sig = Ugen(sig);
        bus_sender.send(sig.busses("")).expect("can't get busses");
        while stop.try_recv().is_err() {
            let sig2 = &mut sig * env.clone();
            for sample in sig2 {
                tx_audio.send(sample).expect("rx is dead");
            }
            pause.recv().expect("can't restart");
        }
    });
}

impl<Sig: f_input + Clone + Send + 'static, Env: f_input + Clone + Send + 'static>
    Spawner<f32, Sig, Env> for Sig
{
    fn spawn(self, size: usize, env: Env, pause: Receiver<()>, toggle_pause: Sender<()>) -> Synth {
        let (tx_audio, rx_audio) = sync_channel(size);
        let (toggle_stop, stop) = channel();
        let (give_dict, get_dict) = channel();
        pr_spawn(self, stop, pause, tx_audio, give_dict, env);
        Synth {
            api: SynthApi {
                params: get_dict.recv().unwrap(),
                toggle_pause,
                toggle_stop,
            },
            channel: rx_audio,
        }
    }
}

pub fn spawn3(
    txs: Vec<SenderEnum>,
    ugen: PluginRunner,
    env: Envelope<impl f_input + Send + 'static>,
) -> Synth {
    let (toggle_stop, stop) = channel();
    let (give_dict, get_dict) = channel();
    let (_, rx_audio) = channel();
    pr_spawn2(ugen, stop, env.1, txs, give_dict, env.2);
    Synth {
        api: SynthApi {
            params: get_dict.recv().unwrap(),
            toggle_pause: env.0,
            toggle_stop,
        },
        channel: rx_audio,
    }
}

fn pr_spawn2<T: f_input + Clone + Send + 'static, Env: f_input + Clone + Send + 'static>(
    sig: T,
    stop: Receiver<()>,
    pause: Receiver<()>,
    outs: Vec<SenderEnum>,
    bus_sender: Sender<HashMap<String, Bus>>,
    env: Env,
) {
    std::thread::spawn(move || {
        let mut sig = Ugen(sig);
        bus_sender.send(sig.busses("")).expect("can't get busses");
        while stop.try_recv().is_err() {
            let sig2 = &mut sig * env.clone();
            for sample in sig2 {
                for out in &outs {
                    out.send(sample);
                }
            }
            pause.recv().expect("can't restart");
        }
    });
}
