extern crate cpal;

pub mod master;
pub mod plug;
pub mod sender_trait;
pub mod spawn;
pub mod synth;
