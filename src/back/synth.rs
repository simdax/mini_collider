use crossbeam::{channel::SendError, Receiver, Sender};
use std::collections::HashMap;
use ugens::*;

pub struct Synth {
    pub channel: Receiver<f32>,
    pub api: SynthApi,
}

#[derive(Debug, Serialize, Clone)]
pub struct SynthApi {
    pub params: HashMap<String, Bus>,
    #[serde(skip)]
    pub toggle_stop: Sender<()>,
    #[serde(skip)]
    pub toggle_pause: Sender<()>,
}

impl SynthApi {
    pub fn send(&self, msg: &str) -> Result<(), String> {
        match &msg[..] {
            "stop" => self.stop().map_err(|_| "send error".to_owned()),
            "pause" => self.toggle().map_err(|_| "send error".to_owned()),
            "toggle" => self.toggle().map_err(|_| "send error".to_owned()),
            _ => {
                let (key, value) = self.get_msg(msg);
                match self.params.get(&key) {
                    Some(bus) => match value {
                        Some(v) => {
                            bus.send(v);
                            Ok(())
                        }
                        None => Err(format!("no ptr for param: {}", key)),
                    },
                    None => Err(format!("no key for param: {}. msg: {:?}", key, msg)),
                }
            }
        }
    }
    pub fn toggle(&self) -> Result<(), SendError<()>> {
        self.toggle_pause.send(())
    }
    pub fn stop(&self) -> Result<(), SendError<()>> {
        self.toggle()?;
        self.toggle_stop.send(())?;
        self.toggle()
    }
    fn get_msg(&self, msg: &str) -> (String, Option<f32>) {
        let mut msg: Vec<&str> = msg.split(' ').collect();
        let mut val = None;
        if let msg = msg.pop().unwrap() {
            val = msg.parse().ok();
        }
        (msg.join(" "), val)
    }
}
