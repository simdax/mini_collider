use std::sync::mpsc::SyncSender;

#[derive(Debug, Clone)]
pub enum SenderEnum {
    Master(SyncSender<f32>),
    Ws(ws::Sender),
}

impl SenderTrait for SenderEnum {
    fn send(&self, v: f32) {
        match self {
            SenderEnum::Master(s) => s.send(v).expect("rx is dead"),
            SenderEnum::Ws(s) => s.send(format!("{{\"out\": {}}}", v)).expect("rx is dead"),
        };
    }
}

pub trait SenderTrait: Clone + Sync + Send {
    fn send(&self, v: f32);
}

macro_rules! impl_sender_trait {
    ($type:ident) => {
        impl SenderTrait for $type<f32> {
            fn send(&self, v: f32) {
                self.send(v).expect("rx is dead");
            }
        }
    };
}
impl_sender_trait!(SyncSender);
