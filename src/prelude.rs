extern crate ugens;
extern crate ws as extern_ws;
use extern_ws::{listen, Error, Message};
use std::thread::JoinHandle;

use self::ugens::json::Json;
use crate::back::master::Master;
use crate::back::plug::Plug;
use crate::back::spawn::Spawner;
use crate::server as startServer;
use crate::synth::SynthApi;
use crate::SERVER_URL;
use ugens::{f_input, Envelope};

pub fn start<U: f_input + Clone + Send + 'static, T: f_input + Clone + Send + 'static>(
    ugen: U,
    env: Envelope<T>,
) -> SynthApi {
    let audio = Master::default();
    let synth = ugen.spawn(2000, env.2, env.1, env.0);
    audio.plug(&synth);
    synth.api
}

pub fn server<U: f_input + Clone + Send + 'static, T: f_input + Clone + Send + 'static>(
    ugen: U,
    env: Envelope<T>,
) {
    let api = start(ugen, env);
    ws(api);
    startServer(SERVER_URL);
}

pub fn ws(api: SynthApi) -> JoinHandle<Result<(), Error>> {
    std::thread::spawn(move || {
        listen(SERVER_URL, |out| {
            out.send(api.params.json()).expect("can't send schema");
            let api = api.clone();
            println!("{:?}", api);
            move |msg: Message| {
                if let msg = msg.as_text().unwrap() {
                    match api.send(msg) {
                        Err(e) => out.send(e),
                        _ => out.send(""),
                    }
                } else {
                    out.send("pb")
                }
            }
        })
    })
}
