#![allow(warnings)]

use mini_collider::sender_trait::SenderEnum;
use mini_collider::ws::{send_api, websocket};
use mini_collider::{
    copy_dll, fft, server as startServer, watch::watch, MiniCollider, LIB_PATH, SERVER_URL,
};
use notify::{RecommendedWatcher, RecursiveMode, Watcher};
use std::sync::mpsc::{sync_channel, SyncSender};
use std::sync::{Arc, Mutex};
use std::thread;
use ugens::json::Json;

fn ws_send_sound(out: ws::Sender) -> SyncSender<f32> {
    let block_size = 2048;
    let (tx, rx) = sync_channel(block_size);
    thread::spawn(move || loop {
        let data = rx.iter().take(block_size).collect::<Vec<f32>>();
        let data_json = data.json();
        let fft = fft(data);
        out.send(format!("{{\"out\": {}}}", data_json)).expect("");
        out.send(format!("{{\"fft\": {}}}", fft.json())).expect("");
    });
    tx
}

fn plugins_watch() -> Result<(), String> {
    let cur = std::env::current_dir().map_err(|e| e.to_string())?;
    std::env::set_current_dir("plugins").map_err(|e| e.to_string())?;
    std::thread::spawn(|| watch().map_err(|e| e.to_string()).expect(""));
    std::env::set_current_dir(&cur).map_err(|e| e.to_string())
}

fn dll_watcher(
    app: Arc<Mutex<MiniCollider>>,
    out: ws::Sender,
) -> Result<RecommendedWatcher, String> {
    Watcher::new_immediate(move |res| match res {
        Ok(event) => {
            println!("reload: {:?}", event);
            let mut app = app.lock().unwrap();
            let dll_path = copy_dll(LIB_PATH);
            app.stop_plugin();
            app.refresh(dll_path);
            app.start_plugin().unwrap_or_else(|e| println!("{}", e));
            send_api(out.clone(), &app.api, &app.plugins).expect("can't send");
        }
        Err(e) => println!("watch error: {:?}", e),
    })
    .map_err(|e| e.to_string())
}

fn main() -> Result<(), String> {
    let app = Arc::new(Mutex::new(MiniCollider::new()?));
    let ws_out = websocket(app.clone()).map_err(|e| e.to_string())?;
    let ws_watcher = ws_send_sound(ws_out.clone());
    {
        let mut app = app.lock().unwrap();
        app.outs.push(SenderEnum::Master(ws_watcher));
    }
    // plugins_watch()?;
    let mut w = dll_watcher(app, ws_out)?;
    w.watch(LIB_PATH, RecursiveMode::Recursive)
        .map_err(|e| e.to_string())?;
    startServer(SERVER_URL);
    Ok(())
}
