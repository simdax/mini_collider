pub mod ws;

use std::fs;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::net::TcpListener;

pub fn server(addr: &str) {
    println!("launching server @ {}", addr);
    let listener = TcpListener::bind(addr).unwrap();
    for connection in listener.incoming() {
        if let stream = connection.expect("") {
            let mut buffer = String::new();
            let mut stream_write = BufWriter::new(stream.try_clone().expect("can't clone"));
            let mut stream_read = BufReader::new(stream);
            stream_read.read_line(&mut buffer).expect("can't reaf");
            if let page = fs::read_to_string("src/front/front.html").expect("can't read friont") {
                let page = format!("HTTP/1.1 200 OK\r\n\r\n{}", page);
                let page = page.as_bytes();
                stream_write.write_all(page).expect("can't write");
                stream_write.flush().expect("can't flush");
            }
        }
    }
}
