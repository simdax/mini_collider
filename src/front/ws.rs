extern crate ws;
use self::ws::WebSocket;
use crate::back::synth::SynthApi;
use crate::MiniCollider;

use std::sync::{Arc, Mutex};
use ugens::json::Json;

pub fn send_api(out: ws::Sender, api: &Option<SynthApi>, plugins: &[String]) -> ws::Result<()> {
    out.send(format!("{{\"plugins\": {} }}", plugins.json()))?;
    if let Some(api) = api {
        out.send(api.json())?;
    }
    Ok(())
}

pub fn websocket(app: Arc<Mutex<MiniCollider>>) -> Result<ws::Sender, ws::Error> {
    let factory = move |out: ws::Sender| {
        println!("received connection");
        let app = app.clone();
        {
            let app = app.lock().unwrap();
            send_api(out.clone(), &app.api, &app.plugins).expect("");
        }
        move |msg: ws::Message| {
            let res = msg
                .as_text()
                .map_or(String::from("can't get message"), |msg| -> String {
                    match &(msg.split(' ').collect::<Vec<&str>>())[..] {
                        ["plugin", ..] => msg.split(' ').nth(1).map_or(
                            String::from("no plugin available"),
                            |name| {
                                let mut app = app.lock().unwrap();
                                app.stop_plugin();
                                app.actual_ugen = Some(name.parse().unwrap());
                                match app.start_plugin() {
                                    Ok(()) => {
                                        send_api(out.clone(), &app.api, &app.plugins)
                                            .expect("can't send");
                                        String::new()
                                    }
                                    Err(e) => e,
                                }
                            },
                        ),
                        _ => app.lock().map_or(String::from("can't unlock"), |app| {
                            app.api
                                .as_ref()
                                .map_or(String::from("no api available"), |api| {
                                    api.send(msg)
                                        .map_or(String::from("can't send"), |_| String::new())
                                })
                        }),
                    }
                });
            out.send(res)
        }
    };
    let ws = WebSocket::new(factory)?;
    let out = ws.broadcaster();
    std::thread::spawn(move || {
        ws.listen("127.0.0.1:8000").expect("websocket can't listen");
    });
    Ok(out)
}
