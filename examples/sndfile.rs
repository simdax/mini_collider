#![feature(never_type)]

use cpal::SampleRate;
use mini_collider::*;
use ugens::*;

fn main() {
    // control_busses! {busses, mix = 0.1, room = 0.1, damp = 0.5, amp2 = 0.8};
    // let sr = SampleRate(48000);
    // let music = SndFile::new("examples/assets/music.wav", sr);
    // let voice_file = SndFile::new("examples/assets/voice.wav", sr);
    // let voice_delay = Delay::new(voice_file.clone(), 5000);
    // let voices = FreeVerb::new(voice_file, mix, room, damp);
    // let sig = voices.chain(voice_delay + music * amp2).cycle();
    // start(sig, Envelope::default());
}
