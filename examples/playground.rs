// original SuperCollider implementation

// let env, snd, vibrato, tremolo, mod2, mod3;
// var env, snd, vibrato, tremolo, mod2, mod3;
// env = Env.adsr(att, dec, sus, rel).kr(gate: gate);
// vibrato = SinOsc.kr(vibratoRate).range(freq * (1 - vibratoDepth), freq * (1 + vibratoDepth));
// tremolo = LFNoise2.kr(1).range(0.2, 1) * SinOsc.kr(tremoloRate).range((1 - tremoloDepth), 1);
// snd = SinOsc.ar(freq: [freq, vibrato], mul:(env * tremolo * amp)).distort;
// snd = Mix.ar([snd]);
// snd = FreeVerb.ar(snd, reverbMix, roomSize, damp);
// DetectSilence.ar(snd, 0.0001, 0.2, doneAction: 2);
// Out.ar(out, Pan2.ar(snd, pan));

use mini_collider::*;
use plugins::*;

fn main() {
    // control_busses! { busses,
    //     //Standard Values:
    //     // out = 0, pan = 0,
    //     freq = 440, amp = 0.5,
    //     att = 0.4, dec = 0.5, sus = 0.8, rel = 1.0,
    //     //Other Controls:
    //     vibrato_rate = 4, vibrato_depth = 0.015, tremolo_rate = 5,
    //     //These controls go from 0 to 1:
    //     tremolo_depth = 0.5, reverb_mix = 0.5, room_size = 1, damp = 0.5
    // };
    // let vibrato = Sin::new(Phasor::new(vibrato_rate), 1.0)
    //     .range(1.0 - (vibrato_depth * freq), 1.0 + (vibrato_depth * freq));
    // let tremolo = Noise {}.range(0.2, 1.0)
    //     * Sin::new(Phasor::new(tremolo_rate), 1.0).range(1.0 - tremolo_depth, 1.0);
    // let snd = (Sin::new(Phasor::new(freq), tremolo.clone() * amp)
    //     + Sin::new(Phasor::new(vibrato), tremolo * amp))
    // .distort();
    // let snd = FreeVerb::new(snd, reverb_mix, room_size, damp);
    // let sr = 48000.0;
    // let env = adsr(att * sr, dec * sr, sus, rel * sr);
    // server(snd, env);
}
