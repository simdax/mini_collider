#![feature(tau_constant)]
#![feature(trait_alias)]

use mini_collider::*;

fn sin1(freq: impl UgenTrait, amp: impl UgenTrait) -> impl UgenTrait {
    FastSin {
        phasor: Line::new(TAU / 48000.0, 0.0) * freq,
        amp,
    }
}

fn sin2(freq: impl UgenTrait, amp: impl UgenTrait) -> impl UgenTrait {
    Sin {
        phasor: Phasor::new(freq),
        amp,
    }
}

fn main() {
    // control_busses!(freq_mod = 10, amp_mod = 0.6, freq = 440, amp = 0.15);
    // let ugen = sin2(freq * sin1(freq_mod, amp_mod), amp);
    // server(ugen, Envelope::default());
}
