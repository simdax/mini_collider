extern crate proc_macro;
use proc_macro::TokenStream;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;

const FILE_NAME: &str = ".plugins";

#[proc_macro]
pub fn new_scalar(stream: TokenStream) -> TokenStream {
    match stream
        .to_string()
        .split(',')
        .collect::<Vec<&str>>()
        .as_slice()
    {
        [a, b] => {
            let idents_array: Vec<&str> = a.split(' ').collect();
            let concat_ident: String = idents_array.join("");
            let res = format!(
                "let mut {} = Scalar::new({},\"{}\");",
                idents_array.first().unwrap(),
                b,
                concat_ident
            );
            println!("{}", res);
            res.parse().unwrap()
            // format!("let mut {}", a.join("")) + b;
        }
        _ => panic!(),
    }
}

#[proc_macro]
pub fn start(_stream: TokenStream) -> TokenStream {
    std::fs::remove_file(FILE_NAME).unwrap_or(());
    File::create(FILE_NAME).expect("can't create file");
    "".parse().unwrap()
}

#[proc_macro]
pub fn add_synth(stream: TokenStream) -> TokenStream {
    let word = stream.into_iter().next().unwrap().to_string();
    println!("found synth : {} ", word);
    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .open(FILE_NAME)
        .unwrap();
    if let Err(e) = writeln!(file, "{}", word) {
        eprintln!("Couldn't write to file: {}", e);
    }
    "".parse().unwrap()
}
