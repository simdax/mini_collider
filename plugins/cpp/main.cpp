#include <iostream>

extern "C" {
    void *Sin();
    float pal_next(void *);
    int pal_set(void *, char *param, float value);
}

int main(){
    auto sin = Sin();
    if (!pal_set(sin, "amp", 0.2))
    {
        int i = 0;
        while (i < 1000)
        {
            std::cout << pal_next(sin) << std::endl;
            ++i;
        }
    }
}