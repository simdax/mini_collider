#![allow(unused_mut)]

#[macro_export]
macro_rules! control_busses {
    ($($name:ident $($rest:ident)* = $value:expr),+) => {
        $(let mut $name = Scalar::new($value as f32, stringify!($name $($rest)*));)*
    };
    ($busses:ident, $($name:ident = $value:expr),+) => {
        use std::collections::HashMap;

        control_busses!($($name = $value),*);
        let busses: HashMap<String, Value> = [
            $((stringify!($name), &mut$name)),*
        ]
        .iter_mut()
        .map(|(name, scalar)| (String::from(*name), scalar.1))
        .collect();
    };
}

#[macro_export]
macro_rules! synth_def {
    ($name:ident, $create:ident) => {
        add_synth!($name);
        #[no_mangle]
        pub extern "C" fn $name() -> Plugin {
            use crate::{Plugin, PluginWrapper};

            let ugen = PluginWrapper::new($create());
            let plugin: Plugin = Box::into_raw(Box::new(Box::new(ugen)));
            unsafe {
                plugin.as_mut().unwrap().refresh();
            }
            plugin
        }
    };
}
