#![feature(box_syntax)]
#![feature(tau_constant)]
#![feature(trait_alias)]
#![allow(warnings)]

extern crate synth_def;
pub use synth_def::*;
start!();

pub mod macros;
mod plugin;
pub use plugin::*;

unsafe fn get_plugin(plugin: Plugin) -> Result<&'static mut Box<dyn PluginTrait>, ()> {
    match plugin.as_mut() {
        Some(plugin) => Ok(plugin),
        None => {
            println!("no running plugin");
            Err(())
        }
    }
}

/// # Safety
#[no_mangle]
pub unsafe extern "C" fn pal_next(plugin: Plugin) -> f32 {
    match get_plugin(plugin) {
        Ok(plugin) => plugin.next(),
        Err(()) => 0.0,
    }
}

/// # Safety
#[no_mangle]
pub unsafe extern "C" fn pal_set(plugin: Plugin, param: *const c_char, value: f32) -> i32 {
    match get_plugin(plugin) {
        Ok(plugin) => {
            let key = CStr::from_ptr(param).to_str().unwrap();
            match plugin.set(key, value) {
                Ok(()) => 0,
                Err(e) => {
                    println!("{}", e);
                    2
                }
            }
        }
        Err(()) => 1,
    }
}

pub use ugens::*;
mod synths;
use std::ffi::CStr;
use std::os::raw::c_char;
pub use synths::*;
