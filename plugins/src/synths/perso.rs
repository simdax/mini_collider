use crate::*;

macro_rules! sin {
    ($i:tt, $fr:expr, $freq: expr) => {{
        control_busses!(amp $i = 0.012, freq $i = $fr);
        let s = FastSin {
            phasor: Line::new(freq * $freq, 0.0),
            amp
        };
        println!("{:?}", s);
        s
    }};
}

pub fn multi() -> impl UgenTrait {
    control_busses!(amp = 0.5, gen_freq = 1);
    let a = (sin!(a, 0.1, gen_freq) + sin!(b, 0.1, gen_freq) + sin!(c, 0.1, gen_freq)) * amp;
    println!("{:?}", a);
    a
}

synth_def!(Testa, multi);
