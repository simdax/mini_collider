use crate::*;

pub fn sin_synth() -> impl UgenTrait {
    Sin {
        phasor: Line::new(
            Sin {
                phasor: Line::new(Scalar::new(TAU / 48000.0, "freq"), 0.0),
                amp: Scalar::new(0.5 * TAU / 48000.0, "freq_amp"),
            },
            0.0,
        ),
        amp: Scalar::new(0.1, "amp"),
    }
}

synth_def! { Sin, sin_synth}

fn sin1(freq: impl UgenTrait, amp: impl UgenTrait) -> impl UgenTrait {
    FastSin {
        phasor: Line::new(TAU / 48000.0, 0.0) * freq,
        amp,
    }
}
fn sin2(freq: impl UgenTrait, amp: impl UgenTrait) -> impl UgenTrait {
    Sin {
        phasor: Phasor::new(freq),
        amp,
    }
}
fn sin3() -> impl UgenTrait {
    control_busses!(freq_mod = 10, amp_mod = 0.4, freq = 818, amp = 0.1231);
    sin2(freq * sin1(freq_mod, amp_mod), amp)
}

synth_def! { trucDeOuf, sin3}
