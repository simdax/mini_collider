use std::collections::HashMap;
use ugens::{Bus, Gettable};

pub struct PluginWrapper<I: Iterator<Item = f32> + Gettable> {
    pub plugin: I,
    busses: HashMap<String, Bus>,
}

impl<I: Iterator<Item = f32> + Gettable> PluginWrapper<I> {
    pub fn new(plugin: I) -> Self {
        Self {
            plugin,
            busses: HashMap::new(),
        }
    }
}

pub trait PluginTrait {
    fn parameters(&self) -> HashMap<String, Bus>;
    fn refresh(&mut self);
    fn next(&mut self) -> f32;
    fn set(&mut self, param: &str, value: f32) -> Result<(), String>;
}

impl<I: Iterator<Item = f32> + Gettable> PluginTrait for PluginWrapper<I> {
    fn parameters(&self) -> HashMap<String, Bus> {
        self.busses.clone()
    }
    fn refresh(&mut self) {
        let busses = self.plugin.busses("");
        self.busses = busses;
    }
    fn next(&mut self) -> f32 {
        self.plugin.next().unwrap_or_default()
    }
    fn set(&mut self, param: &str, value: f32) -> Result<(), String> {
        match self.busses.get(param) {
            Some(param) => {
                    param.send(value);
                Ok(())
            }
            None => Err(format!(
                "can't find {} into these available keys : {:?}",
                param,
                self.busses.keys()
            )),
        }
    }
}

impl<I: Iterator<Item = f32> + Gettable> Drop for PluginWrapper<I> {
    fn drop(&mut self) {
        println!("drop plugin");
    }
}

pub type Plugin = *mut Box<dyn PluginTrait>;
